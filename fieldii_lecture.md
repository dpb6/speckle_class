# Field II

- http://field-ii.dk/
- http://field-ii.dk/?./downloading_8_20.html
- http://www.simplehelp.net/2015/08/11/how-to-open-tar-gz-files-in-windows-10/
- http://www.7-zip.org/download.html
- http://field-ii.dk/?examples.html
- If you are a member of the Duke Ultrasound Gitlab group: https://gitlab.oit.duke.edu/ultrasound/Field_II_Pro


## Some notes

Field II doesn't do: 

- varying material properties: sound speed/attenuation/etc
- aberration
- multiple reflection/scattering
- harmonic/nonlinear/shock wave
- FEM/shear wave/heating

Field II does do: 

- arbitrary apertures
- element-specific transmit functions
- dynamic receive focus, dynamic apodization
- synthetic aperture imaging
- flow simulation


## Homework

You are given the following transducer array specifications:

- Linear array
- 128 elements
- 5 MHz center frequency
- 75% bandwidth (BW)
- lambda/2 pitch (half a wavelength distance between element centers)
- Elevation lens with a fixed focus at 5 cm depth

Complete the following exercises:

1. Do a Field ii simulation of the point spread function (PSF) of point target at 5 cm depth
  1. Find the -6, -12, and -20 dB lateral resolution
2. Do another simulation of a 1 cm anechoic cyst lesion, centered at 5 cm depth
  1. Find the lesion contrast and Contrast-to-Noise ratio (CNR)
3. Then change the following parameters and redo the measurements above:
  1. Omit or disable every other element in the array, keeping all else the same
  2. Reduce BW to 25%
  3. Shrink aperture (number of elements used) by 2
  4. change acoustic velocity by +5% and -5%

## Office hours

Dr. Bradway (dpb6@duke.edu) will be available by appointment on Friday and Tuesday in Hudson Annex 251.

## Report

In your resulting report, include sample plots and images, tables of results for the different cases, and discussions of your findings for each simulation and experiment.

Include a link to your source code repository or folder (Use https://gitlab.oit.duke.edu/, https://box.duke.edu/ etc.)

In your report, cite any sample code you download to use, and reference the Field II journal articles (http://field-ii.dk/?copyright.html).

## Deadline

This assignment is due 01/19/2017 at 11:59 PM EST. Submit a PDF to Dr. Bradway (dpb6@duke.edu).
